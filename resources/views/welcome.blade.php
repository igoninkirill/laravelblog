@extends('adminlte::page')

@section('title', 'Dashboard')

<link rel="stylesheet" link="cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
<script link="cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>


@section('content_header')
    <h1>Страница</h1>
@stop


@section('content')
    <p>Добро пожаловать.</p>
    <script> 
        $(document).ready( function () {
            $('#myTable').DataTable();
        } );
    </script>
@section('plugins.Datatables', true)    
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop