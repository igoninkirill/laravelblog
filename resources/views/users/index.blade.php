@extends('adminlte::page')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="testmessage" id="testmessage">
            @include('flash::message')
            </div>
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Таблица пользователей</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    {!! $dataTable->table(['class' => 'table table-bordered table-hover']) !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script src="https://adminlte.io/themes/dev/AdminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>


{!! $dataTable->scripts() !!}
<script>

function deleteItem(id) {
    var _token = $("input[name='_token']").val();
    $.ajax({
        type: "POST",
        url: '/users/'+id,
        data: '_method=DELETE&_token=' + _token,
        success: function (result) {
            console.log(result);
            $('#dataTableBuilder').DataTable().ajax.reload();
            $('#testmessage').text("@include('flash::message')");
        }
    });
    
}

$('#flash-overlay-modal').modal();

$('div.alert').not('.alert-important').delay(2000).slideUp(800);

</script>

@endsection


