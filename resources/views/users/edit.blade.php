@extends('adminlte::page')

@section('content') 

<div class="col-md-6">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Форма редактирования</h3>
        </div>
        <form action="{{ URL::route('users.update', ['user' => $user->id]) }}" method="post">
            @csrf
            <input type="hidden" name="_method" value="PUT">
            <div class="card-body">
                <div class="form-group">
                    <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">Изменить имя</label>
                        <input class="form-control {{ ($errors->has('name')) ? 'is-invalid' : null  }}" type="text" name="name" id="inputName" value="{!! $user->name !!}">
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-sm-4 control-label">Изменить email</label>
                        <input class="form-control {{ ($errors->has('email')) ? 'is-invalid' : null  }}" type="email" name="email" id="inputEmail" value="{!! $user->email !!}">
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="col-sm-4 control-label">Изменить пароль</label>
                        <input class="form-control {{ ($errors->has('passwords')) ? 'is-invalid' : null  }}" type="password" name="password" id="inputPassword" placeholder="Введите новый пароль">
                    </div>
                    <div class="form-group">
                    <label class="col-sm-2 control-label">Изменить роль</label>
                    <select name="role" class="custom-select">
                        @foreach($roles as $role)
                            <option value="{{ $role->name }}">{{ $role->name }}</option>
                        @endforeach
                    </select>
                    </div>
                    <div class="form-group">
                        @if(isset($nameRole))
                            <b>Текущая роль: </b>{{ $nameRole->name }}
                        @endif
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Редактировать</button>
            </div>
        </form>
    </div>
</div>
@endsection

