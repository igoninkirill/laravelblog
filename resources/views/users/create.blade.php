@extends('adminlte::page')

@section('content')

<div class="col-md-6">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Создать пользователя</h3>
        </div>
        <form action="{{ URL::route('users.store') }}" method="post">
            @csrf
            <div class="card-body">
                <div class="form-group">     
                    <div class="form-group">
                        <div class="form-group">
                            <label for="inputName" class="col-sm-2 control-label">Имя</label>
                            <input class="form-control {{ ($errors->has('name')) ? 'is-invalid' : null  }}" type="text" name="name" id="inputName" placeholder="Введите имя">
                        </div>
                        <div class="form-group">
                            <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                            <input class="form-control {{ ($errors->has('email')) ? 'is-invalid' : null  }}" type="email" name="email" id="inputEmail" placeholder="Введите email">
                        </div>
                        <div class="form-group">
                            <label for="inputPassword" class="col-sm-2 control-label">Пароль</label>
                            <input class="form-control {{ ($errors->has('password')) ? 'is-invalid' : null  }}" type="password" name="password" id="inputPassword" placeholder="Введите пароль">
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Создать</button>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </div>
        </form>
    </div>
</div>
@endsection

