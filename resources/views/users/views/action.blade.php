 
 @role('admin')
<button class="btn btn-danger" onclick="deleteItem({{ $user->id }})" id="button_datatables" type="button"><i class="fas fa-trash-alt"></i></button>

<a href="{{ URL::route('users.edit', ['user' => $user->id]) }}" ><button class="btn btn-success" ><i class='fas fa-user-edit'></i></button></a> 
@endrole