<?php

namespace App\Http\Controllers;

use Yajra\DataTables\Html\Builder;

class DatatablesController extends Controller
{
    public function index(Builder $builder) 
    {
        if (request()->ajax()) {
            return DataTables::of(User::query())->toJson();
        }
        
        $html = $builder->columns([
                    ['data' => 'id', 'name' => 'id', 'title' => 'Id'],
                    ['data' => 'name', 'name' => 'name', 'title' => 'Name'],
                    ['data' => 'email', 'name' => 'email', 'title' => 'Email'],
                    ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Created At'],
                    ['data' => 'updated_at', 'name' => 'updated_at', 'title' => 'Updated At'],
                ]);

        return view('users.index', compact('html'));
    }
}   

/** 
 
function(Builder $builder) {
    if (request()->ajax()) {
        return DataTables::of(User::query())->toJson();
    }

    $html = $builder->columns([
                ['data' => 'id', 'name' => 'id', 'title' => 'id'],
                ['data' => 'name', 'name' => 'name', 'title' => 'Имя'],
                ['data' => 'email', 'name' => 'email', 'title' => 'Email'],
                ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Создан'],
                ['data' => 'updated_at', 'name' => 'updated_at', 'title' => 'Обновлен'],
            ]);
           

    return view('users.index', compact('html'));
}); 

*/