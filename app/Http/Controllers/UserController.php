<?php

namespace App\Http\Controllers;

use App\User;
use Yajra\DataTables\Html\Builder;
use App\DataTables\UsersDataTable;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\DB;


class UserController extends Controller
{
    protected $arrRole = ['admin', 'manager', 'employee'];

    public function index(User $user, UsersDataTable $dataTable) 
    {   
        if (request()->ajax()) {
            return Datatables::of(User::select(['id', 'name', 'email', 'created_at', 'updated_at']))
            ->editColumn('action', function($user) {
                return view('users.views.action', ['user' => $user]);
            })
            ->editColumn('created_at', function ($request) {
                return $request->created_at; /** getter */
            })
            ->editColumn('updated_at', function ($request) {
                return $request->updated_at;
            })
            ->make(true);
        }
        /*if (Auth::user()->hasRole('admin|manager')){ */
            return $dataTable->render('users.index');
        
    }

    public function store(UserRequest $request, User $user)
    {
        User::create($request->all());
        flash()->success('Пользователь успешно создан');
        return redirect()->action('UserController@index');
    }

    public function destroy(User $user)
    {   
        flash()->success('Пользователь успешно удален');
        return $user->delete();
    }

    public function edit(User $user)
    {   $nameRole = DB::table("role_user")
            ->where('user_id', '=', $user->id)
            ->join('roles', 'roles.id', '=', 'role_user.role_id') 
            ->select("roles.name")
            ->first();
        return view('users/edit', ['user' => $user, 'roles' => DB::select('select name from roles'), 'nameRole' => $nameRole]);
    }

    public function update(Request $request, User $user)
    {
        $requestAll = $request->all();

        $arrRole = $this->arrRole;
        if (isset($requestAll['role'])) {
            foreach ($arrRole as $role) {
                if ($user->hasRole($role) == true) {
                    $user->detachRole($role);
                } 
            }
            $user->attachRole($requestAll['role']);
        }
        if ($requestAll['password'] == null){   
            unset($requestAll['password']);
        }
        $user->update($requestAll);   
        flash()->success('Пользователь успешно отредактирован');
        return redirect()->action('UserController@index');
    } 

    public function create()
    {
        return view('users/create');
    }

}


/*     
$arrRole = ['admin', 'manager', 'employee'];
foreach ($arrRole as $role)
{
    if (Auth::user()->hasRole($role) == true) {
        dd($role);
    } 
} 
*/ 