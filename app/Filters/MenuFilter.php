<?php

namespace App\Filters;

use JeroenNoten\LaravelAdminLte\Menu\Filters\FilterInterface;
use Laratrust\Laratrust;
use Illuminate\Support\Facades\Auth;

class MenuFilter implements FilterInterface
{
    public function transform($item)
    {   
        if (isset($item['permission']) && ! Auth::user()->isAbleTo($item['permission'])) {
            $item['restricted'] = true;
        }

        return $item;
    }
}