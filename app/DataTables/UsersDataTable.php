<?php

namespace App\DataTables;

use App\User;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use Carbon\Carbon;

class UsersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d',
        'updated_at' => 'datetime:Y-m-d'
    ];

    public function ajax()
    {
        return datatables()
            ->eloquent($this->query())
            ->make(true);
    }


    /**
     * Get query source of dataTable.
     *
     * @param \App\App\UsersDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $users = User::select('id', 'name', 'email', $this->casts );

        return $this->applyScopes($users);
    }
    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns([
                'id',
                ['data'=>'name', 'title'=>'Имя'],
                'email',
                ['data'=>'created_at', 'title'=>'Создан'],
                ['data'=>'updated_at', 'title'=>'Обновлен'],
                ['data'=>'action', 'title'=>'Действие'],
            ])
            ->parameters([
                'dom' => 'Bfrtip',
                'buttons' => [],
                'language' => ['url' => '//cdn.datatables.net/plug-ins/1.10.22/i18n/Russian.json'],
                'data' => ['url' => '//cdn.datatables.net/plug-ins/1.10.22/sorting/date-uk.js']
            ]);
    }

}

/** <a href='users/create'>Создать</a> 