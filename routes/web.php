<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth;
use Laratrust\Laratrust;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();

Route::post('/', 'Auth\LoginController@login');

Route::get('/', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('/logout', 'Auth\LoginController@logout');
Route::get('/password/confirm', 'Auth\ConfirmPasswordController@showConfirmForm');
Route::post('/password/confirm', 'Auth\ConfirmPasswordController@confirm');
Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::post('/password/reset','Auth\ResetPasswordController@reset');
Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::post('/register', 'Auth\RegisterController@register');
Route::get('/register', 'Auth\RegisterController@showRegistrationForm');


Route::group(['middleware' => ['auth', 'verified']], function () {

    Route::resource('users', 'UserController')->names([
        '/users' => 'users',
        '/users/{user}/edit' => 'users.edit',
        '/users/create' => 'users/create',
        '/users/{user}/update' => 'users.update',
        '/users/{user}' => 'user.delete'
    ]);

});

/*     Route::resource('users', 'UserController', ['middleware' => ['role:admin|manager']])->names([
        '/users' => 'users',
        '/users/{user}/edit' => 'users.edit',
        '/users/create' => 'users/create',
        '/users/{user}/update' => 'users.update',
        '/users/{user}' => 'user.delete'
    ]);

*/